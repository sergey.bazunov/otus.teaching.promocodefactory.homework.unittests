﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public static class PartnerBuilder
    {
        public static Partner CreateBuilder()
        {
            return new Partner();
        }

        public static Partner WithId(this Partner partner, Guid guid)
        {
            partner.Id = guid;
            return partner;
        }

        public static Partner WithName(this Partner partner, string name)
        {
            partner.Name = name;
            return partner;
        }

        public static Partner WithActiveStatus(this Partner partner, bool active)
        {
            partner.IsActive = active;
            return partner;
        }

        public static Partner WithIssuedPromoCodes(this Partner partner, int codes)
        {
            partner.NumberIssuedPromoCodes = codes;
            return partner;
        }

        public static Partner WithLimits(this Partner partner, ICollection<PartnerPromoCodeLimit> limits)
        {
            partner.PartnerLimits = limits;
            return partner;
        }

    }
}
